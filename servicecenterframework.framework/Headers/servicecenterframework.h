//
//  servicecenterframework.h
//  servicecenterframework
//
//  Created by Pavel Sich on 14/04/2019.
//  Copyright © 2019 Lufthansa German Airline. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for servicecenterframework.
FOUNDATION_EXPORT double servicecenterframeworkVersionNumber;

//! Project version string for servicecenterframework.
FOUNDATION_EXPORT const unsigned char servicecenterframeworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <servicecenterframework/PublicHeader.h>


